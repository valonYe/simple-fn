//  拓展函数调用行为(在无装饰器情况下可用, 有则使用装饰器)
//  思想: (SOP编程, 函数式编程-构造函子)
Function.prototype.before = function (fn) {
  return () => {
    const doNext = fn && fn()
    if (doNext === false) return
    return this()
  }
}

Function.prototype.after = function(fn) {
  return () => {
    const res = this()
    fn && fn.apply(this)
    return res
  }
}

function test() {
  //  maybe to do something here...
  console.log(2)
}

test.before(() => {
  console.log(1)
}).after(() => {
  console.log(3)
})();

/* ----------------------------------------------- */

//  抽离对象数组中某个属性作为索引，
//  建立数据索引查找对象(应用: 前端洗数据)
const pullKeyFromArray = (arr = [], key) => {
  const raw = arr.reduce((res, item) => {
    const { [key]: id, ...rest } = item
    if (!id) return
    res[id] = res[id] || []
    //  ⬇️ 防止同属性多对象情况出现 👇
    res[id].push(rest)
    return res
  }, {})
  return raw
}

const arr = [
  { name: 'name', id: 2 },
  { name: 'label', id: 1 },
  { name: 'text', id: 2 },
  { name: 'value', id: 3 }
]
pullKeyFromArray(arr, 'id')

/* ----------------------------------------------- */

//  获取一个URL中的参数
function parseUrl (url = '') {
  if (!url) return null
  const searchRex = /([^=&|?]+)=([^&]*)/gm
  const urlRex = /(\w+)[^:]*?:\/\/([^:|\/]+)([:\d]*)([^?]*)(\S+)/gm
  const obj = {}
  url.replace(urlRex, (str, protocol, host, port, pathname, qs = '') => {
    let search = {}
    if (qs && qs.length) {
      qs.replace(searchRex, (s, key, val) => {
        search[key] = val
      })
    }
    let _obj = {
      protocol,
      host,
      port,
      pathname,
      search
    }
    Object.assign(obj, _obj)
  })
  return obj
}